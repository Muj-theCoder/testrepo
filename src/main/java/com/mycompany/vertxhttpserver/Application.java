/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vertxhttpserver;

import io.vertx.core.Vertx;

/**
 *
 * @author mujtaba.sarwari
 */
public class Application {
    
    public static void main(String[] args){
        
        Vertx vertx=Vertx.vertx();
        
        vertx.deployVerticle(new ServerVertical());
        
    }
}
