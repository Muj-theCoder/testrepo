/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.vertxhttpserver;

import io.vertx.core.AbstractVerticle;
import io.vertx.core.Handler;
import io.vertx.core.Verticle;
import io.vertx.core.http.HttpServer;
import io.vertx.core.http.HttpServerRequest;

/**
 *
 * @author mujtaba.sarwari
 */
public class ServerVertical extends AbstractVerticle {

    private HttpServer httpServer = null;
    
    
    
    
    @Override
    public void start() {

        vertx.createHttpServer().requestHandler(new Handler<HttpServerRequest>() {
            
            @Override
            public void handle(HttpServerRequest httpServerRequest) {
                httpServerRequest.response().end("Hello you");
            }
        }).listen(8888);

       

    }

}
